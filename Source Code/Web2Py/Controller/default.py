# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
import subprocess
def index():
    return locals()

def user():
    return dict(form=auth())

def feeding():
    db.weekdays.update_or_insert(day="Monday")
    db.weekdays.update_or_insert(day="Tuesday")
    db.weekdays.update_or_insert(day="Wednesday")
    db.weekdays.update_or_insert(day="Thursday")
    db.weekdays.update_or_insert(day="Friday")
    db.weekdays.update_or_insert(day="Saturday")
    db.weekdays.update_or_insert(day="Sunday")
    form = SQLFORM(db.time)
    if form.process().accepted:
        response.flash = 'form accepted'
    elif form.errors:
        response.flash = 'form has errors'
    else:
        response.flash = 'please fill out the form'
    return dict(form=form)

def test():
    rows = db().select(db.day.id)
    return dict()

def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)

def all_records():
    grid = SQLFORM.grid(db.weekdays,user_signature=False)
    return locals()

def camera():
    return dict()

def display_your_form():
    return dict(form = SQLFORM.grid(db.weekdays,user_signature=False,csv=False, deletable=False, create=True, searchable=False, details=False, sortable=False))
     

def information():
    return locals()

def schaeferhund():
    return locals()

def chihuahua():
    return locals()

def yorkshire():
    return locals()

def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()
