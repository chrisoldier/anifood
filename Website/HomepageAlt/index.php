<?php
	if (isset($_POST["submit"])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$human = intval($_POST['human']);
		$from = 'AniFood'; 
		$to = 'Christopher.Wildzeisz@live.de'; 
		$subject = 'AniFood - Kontaktanfrage ';
		
		$body ="From: $name\n E-Mail: $email\n Message:\n $message";
		// Check if name has been entered
		if (!$_POST['name']) {
			$errName = 'Please enter your name';
		}
		
		// Check if email has been entered and is valid
		if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Please enter a valid email address';
		}
		
		//Check if message has been entered
		if (!$_POST['message']) {
			$errMessage = 'Please enter your message';
		}
		//Check if simple anti-bot test is correct
		if ($human !== 5) {
			$errHuman = 'Your anti-spam is incorrect';
		}
// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
	if (mail ($to, $subject, $body, $from)) {
		$result='<div class="alert alert-success">Thank You! I will be in touch</div>';
	} else {
		$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later.</div>';
	}
}
	}
?>


<!DOCTYPE html>
<html lang "en">
<head>
    <meta name="keywords" content="Anifood, food, any ,animals">
    <meta name="description" content="Anifood webstie">
    <title>Anifood</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="AniFood" />
	<meta name="publisher" content="AniFood" />
	<meta name="copyright" content="AniFood" />
	<meta name="date" content="2015-09-30" scheme="Day-Month-Year" />
	
    <link rel="shortcut icon" type="image/ico" href="Logo/anifoodicon.ico"/>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="Untitled.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		// navigation click actions	
		$('.scroll-link').on('click', function(event){
			event.preventDefault();
			var sectionID = $(this).attr("data-id");
			scrollToID('#' + sectionID, 750);
		});
		// scroll to top action
		$('.scroll-top').on('click', function(event) {
			event.preventDefault();
			$('html, body').animate({scrollTop:0}, 'slow'); 		
		});
		// mobile nav toggle
		$('#nav-toggle').on('click', function (event) {
			event.preventDefault();
			$('#main-nav').toggleClass("open");
		});
	});
	// scroll function
	function scrollToID(id, speed){
		var offSet = 50;
		var targetOffset = $(id).offset().top - offSet;
		var mainNav = $('#main-nav');
		$('html,body').animate({scrollTop:targetOffset}, speed);
		if (mainNav.hasClass("open")) {
			mainNav.css("height", "1px").removeClass("in").addClass("collapse");
			mainNav.removeClass("open");
		}
	}
	if (typeof console === "undefined") {
		console = {
			log: function() { }
		};
	}
	</script>
	
	<style>
      #map1 {
        width: 100%;
        height: 400px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var mapCanvas = document.getElementById('map1');
        var mapOptions = {
          center: new google.maps.LatLng(48.1907255, 16.3972948),
          zoom: 17,
		scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		draggable: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(48.1907255, 16.3972948),
			map: map,
			title: 'Hello World!'
  });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
	
  </head>
  <body>

  
  
  
  
      
      
      
      
      
      
      
      
      
      
      <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">AniFood</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
         <li><a href="#" class="scroll-link" data-id="idea">Idee</a>
                </li>
                <li><a href="#" class="scroll-link" data-id="team">Team</a>
                </li>
                <li><a href="#" class="scroll-link" data-id="goals">Ziele</a>
                </li>
                <li><a href="#" class="scroll-link" data-id="doc">Dokumentation</a>
                </li>
				<li><a href="#" class="scroll-link" data-id="contact">Kontakt</a>
                </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="Eindex.php">Englisch</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
      
      
      
      
      
      
      
      
      
      
      
      
  
    <div class="cover">
	  
      
	  
	  
	  
	  
	  
	  
      <div class="cover-image"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h1 class="navigation text-primary">ANIFOOD</h1>
            <p class="text-primary">Sei ein Teil UNSERER Zukunft! </p> 
			<p class="text-primary"> Füttere deine Haustiere auf die komfortable Art.</p>
            <br>
              <audio autoplay>
  <source src="Musik/easy.mp3" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>
            <br>
            <a class="btn btn-lg btn-primary" href="mailto:htl3r.anifood@gmail.com">Bleib in Kontakt!</a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div id="fullcarousel-example1" data-interval="5000" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="active item">
                  <img src="images/toft01.jpg">
                  <!--<div class="carousel-caption">
                    <h2>Title</h2>
                    <p>Description</p> -->
                  </div>
              </div>
            </div>
          </div>
          <div class="col-md-6" id="idea">
            <h1 style="color:#337cbb">Projektidee</h1>
            <h3>Ausgangssituation</h3>
            <p>Viele Menschen halten Haustiere - vor allem Katzen und Hunde - aber es ist schwer seine tägliche Routine an die der Tiere anzupassen.
			Ständig wollen sie mit uns spielen, oder verlangen nach Essen und manchmal wäre das doch viel einfacher, wenn man einfach einen Knopf
			drücken könnte und unser Haustier bekommt ihr Essen, und hier kommt AniFood ins Spiel. Es soll vor allem alten Menschen, die schon etwas
			vergesslich sind helfen, ihr Tier zu füttern. Wenn sie einmal darauf vergessen sollten, bekommen die Tiere ihr Essen einfach von unserer
			Maschine. Enkelkinder oder Kinder können dann ganz einfach die Futterzeit und Futtermenge einstellen und müssen sich nicht jeden Tag Sorgen
			machen, dass Ihre Großeltern das Tier nicht füttern.
			</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
        	
		</div>
          <div class="col-md-6">
            <h3>Beschreibung der Idee</h3>
            <p>AniFood ist nicht nur ein gewöhnlicher Futterspender, er ist mehr als nur das. Die Spende Zeiten lassen
			sich bequem über ein Webinterface einstellen und steuern. Egal ob man sich zuhause befindet oder gerade an 
			einem Geschäftsmeeting teilnimmt. Mithilfe einer Kamera, die sich am Futterspender befindet kann zusätzlich 
			kontrolliert werden, ob das Haustier auch wirklich sein Essen zu sich nimmt. Mithilfe einer eingebauten Waage 
			lässt sich auch die Menge des Essens abwiegen (Portionierung) und nach Bedarf nachgeregelt werden. Soll ihr Tier
			nur die halbe Menge an Futter bekommen, dann kann das auch eingestellt werden, auch das beherrscht der Futterspender
			AniFood. <br>

			Dafür zuständig sind der Raspberry Pi zusammen mit einem WLAN Modul, das mit unserem Webinterface kommuniziert.
			Die Daten werden in Echtzeit übertragen und wichtige Infos, wie Futterstand oder Statistiken angezeigt.
			</p>
          </div>
          <div class="col-md-6">
            <div id="fullcarousel-example2" data-interval="5000" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="item active">
                  <img src="images/toft02.jpg">
                  <!--<div class="carousel-caption">
                    <h2>Title</h2>
                    <p>Description</p>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="team">
      <a name="team">
      </a>
      <div class="container">
        <a href="">
        </a>
        <div class="row">
          <a href="">
          </a>
          <div class="col-md-12">
            <a href="">
            </a>
            <a name="team"></a>
            <h1 class="text-center" style="color:#337cbb">Team</h1>
            <p class="text-center">Wir sind eine Gruppe ambitionierter Arbeiter.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <img src="images/us/leon.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Leon Höfer</h3>
            <p class="text-center">Projektleiter</p>
			<p class="text-center"> Entwickler | Webinterface </p>
          </div>
          <div class="col-md-4">
            <img src="images/us/chris.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Christopher Wildzeisz</h3>
            <p class="text-center">Projektleiter Stellvertreter</p>
			<p class="text-center"> Entwickler | Raspberry </p>
          </div>
          <div class="col-md-4">
            <img src="images/us/weigl.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Mathias Weigl</h3>
			<p class="text-center">Projektmitglied</p>
            <p class="text-center">Entwickler | Maschine</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section bar">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 style="color:#337cbb">Verfolge unseren Fortschritt.</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-right">
            <div class="active bar progress progress-striped">
              <div class="progress-bar progress-bar-info" role="progressbar" style="width: 60%;">
                <a href="#"></a>60% komplett</div>
            </div>
          </div>
        </div>
          <div class="row">
              <div class="col-md-12 center-block">
                <ul class="fa-ul">
                    <p class="lead">
                    <li><i class="fa-li fa fa-check fa-2x"></i>Maschine gebaut</li> <br>
                    <li><i class="fa-li fa fa-check fa-2x"></i>Webinterface programmiert</li> <br>
                    <li><i class="fa-li fa fa-check fa-2x"></i>Waage programmiert</li> <br>
                    <li><i class="fa-li fa fa-check fa-2x"></i>Webcam verbaut</li> <br>
                    <li><i class="fa-li fa fa-spinner fa-spin fa-2x"></i>Feinschliff Webinterface</li> <br>
                    <li><i class="fa-li fa fa-spinner fa-spin fa-2x"></i>Diplomarbeitsbuch</li>
                    </p>
                </ul>
              </div>
          </div>
      </div>
    </div>
    <div class="section" id="goals">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="" class="center-block img-circle img-responsive">
          </div>
          <div class="col-md-6">
            <h1 style="color:#337cbb">Ziele</h1>
            <h3>Ziel ist es, einen vollautomatischen, konfigurierbaren und überwachbaren Futterspender zu bauen.</h3>
            <p>AniFood ist eine „Box“. In ihr befinden sich der Raspberry Pi, eine Waage und eine kleine Kamera.
			Die Futterschüssel kann rausgenommen werden, um sie besser putzen zu können.
			</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3> Ziel ist es, ein Webinterface zu programmieren, welches ermöglicht Futterzeiten einzustellen, 
			den Futterstand in der Schüssel anzeigt und Statistiken über das verbrauchte Futter.
			</h3>
            <p>Im Webinterface gibt es die Möglichkeit, die Hunde- bzw. Katzenrasse anzugeben und daraufhin 
			genauere Informationen zur Ernährung des Tieres zu finden. Diese werden in Infoboxen angezeigt 
			und sollen dem Besitzer helfen, das richtige Futter und vor allem die richtige Menge für sein
			Tier einfach einstellen zu können.</p>
          </div>
          <div class="col-md-6">
            <img src="images/img01.jpg" class="img-circle img-responsive">
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="" class="center-block img-circle img-responsive">
          </div>
          <div class="col-md-6">
            <h3>Ziel ist es, die Futtermaschine „selbst“ zu bauen und keine vorgefertigte Box zu kaufen. </h3>
            <p>Die Futterbox soll aus einem bissfesten Material gebaut werden. Das Material wiederum muss
			dementsprechend behandelt werden. Dazu zählen Abschleifen von Kanten und Ecken und zusätzlich Nässe- und
			Schmutzresistenz. Eine einfache Reinigung soll somit auch ermöglicht sein.
			</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="doc">
	<div class="container">
	<h1 style="color:#337cbb">Dokumentation</h1>
	<br><br>
      <a name="documents"></a>
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-md-offset-1">
            <a name="documents"></a>
            <a href="Downloads/AntragAniFood_SEPT_final_v6.pdf" download=""><img src="images/contract-icon.png" class="img-responsive img-thumbnail"></a>
            <h2>Download Project Antrag</h2>
          </div>
          <div class="col-md-3 col-md-offset-1">
            <a href="#"> <img src="images/1442285252_icon-70-document-file-pdf.png" class="img-responsive img-thumbnail"></a>
            <h2>Download Management Summary</h2>
          </div>
          <div class="col-md-3 col-md-offset-1">
            <a href="#"> <img src="images/1442286506_icon-31-book-bookmark.png" class="img-responsive img-thumbnail"></a>
            <h2>Download project book</h2>
            <p>Coming 2016</p>
          </div>
        </div>
      </div>
    </div>
	</div>
	
	
	
	<div id="map1">			
	    	</div>
		</div><!--/#map-->
	
    <footer class="section section-primary">
	
	
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h1 >Von Menschen, für Tiere</h1>
            <ol class="list-inline">
              <li><h2>Impressum</h2>
<p>
Mathias Weigl<br>
<br>
Josef Anderlikgasse 9<br>
2201<br>
Gerasdorf<br>
Österreich<br>
<br>
069917110625<br>
htl3r.anifood@gmail.com<br>
<br>
</p>
<br>
<a href="tos.html" style="color:#FFF">Terms of Service </a>
</li>
            </ol>
          </div>
		  

		  
          <div class="col-sm-6">
            <p class="footer text-left">
              <br>Find us on
              <b>:</b>
            </p>
            <div class="row">
              <div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 hidden-xs text-left">
                <!-- <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a> -->
                <a href="https://twitter.com/htl3rAniFood"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="https://www.facebook.com/htl3rAniFood"><i class="fa fa-3x fa-facebook fa-fw text-inverse"></i></a>
              </div>
            </div>
			<div class="row">
			
			<div id="contact">
	<h1 style="color:#FFFFFF">Kontakt</h1>
	<form class="form-horizontal" role="form" method="post" action="index.php">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="Vor- & Nachname" value="<?php echo htmlspecialchars($_POST['name']); ?>">
            <?php echo "<p class='text-danger'>$errName</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<?php echo htmlspecialchars($_POST['email']); ?>">
            <?php echo "<p class='text-danger'>$errEmail</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="message" class="col-sm-2 control-label">Nachricht</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="4" name="message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
            <?php echo "<p class='text-danger'>$errMessage</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="human" name="human" placeholder="Antwort">
            <?php echo "<p class='text-danger'>$errHuman</p>";?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary" style="background-color:#000000">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?php echo $result; ?>    
        </div>
    </div>
</form> 
	</div>
			
			</div>
          </div>
		  
        </div>
      </div>
	  
	  
    </footer>
  

</body></html>