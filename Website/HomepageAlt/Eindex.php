<?php
	if (isset($_POST["submit"])) {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$message = $_POST['message'];
		$human = intval($_POST['human']);
		$from = 'Demo Contact Form'; 
		$to = 'Christopher.Wildzeisz@live.de'; 
		$subject = 'Message from Contact Demo ';
		
		$body ="From: $name\n E-Mail: $email\n Message:\n $message";
		// Check if name has been entered
		if (!$_POST['name']) {
			$errName = 'Please enter your name';
		}
		
		// Check if email has been entered and is valid
		if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			$errEmail = 'Please enter a valid email address';
		}
		
		//Check if message has been entered
		if (!$_POST['message']) {
			$errMessage = 'Please enter your message';
		}
		//Check if simple anti-bot test is correct
		if ($human !== 5) {
			$errHuman = 'Your anti-spam is incorrect';
		}
// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
	if (mail ($to, $subject, $body, $from)) {
		$result='<div class="alert alert-success">Thank You! I will be in touch</div>';
	} else {
		$result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later.</div>';
	}
}
	}
?>


<!DOCTYPE html>
<html lang "en">
<head>
    <meta name="keywords" content="Anifood, food, any ,animals">
    <meta name="description" content="Anifood webstie">
    <title>Anifood</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="AniFood" />
	<meta name="publisher" content="AniFood" />
	<meta name="copyright" content="AniFood" />
	<meta name="date" content="2015-09-30" scheme="Day-Month-Year" />
	
    <link rel="shortcut icon" type="image/ico" href="Logo/anifoodicon.ico"/>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="Untitled.css" rel="stylesheet" type="text/css">
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		// navigation click actions	
		$('.scroll-link').on('click', function(event){
			event.preventDefault();
			var sectionID = $(this).attr("data-id");
			scrollToID('#' + sectionID, 750);
		});
		// scroll to top action
		$('.scroll-top').on('click', function(event) {
			event.preventDefault();
			$('html, body').animate({scrollTop:0}, 'slow'); 		
		});
		// mobile nav toggle
		$('#nav-toggle').on('click', function (event) {
			event.preventDefault();
			$('#main-nav').toggleClass("open");
		});
	});
	// scroll function
	function scrollToID(id, speed){
		var offSet = 50;
		var targetOffset = $(id).offset().top - offSet;
		var mainNav = $('#main-nav');
		$('html,body').animate({scrollTop:targetOffset}, speed);
		if (mainNav.hasClass("open")) {
			mainNav.css("height", "1px").removeClass("in").addClass("collapse");
			mainNav.removeClass("open");
		}
	}
	if (typeof console === "undefined") {
		console = {
			log: function() { }
		};
	}
	</script>
	
	<style>
      #map1 {
        width: 100%;
        height: 400px;
      }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
      function initialize() {
        var mapCanvas = document.getElementById('map1');
        var mapOptions = {
          center: new google.maps.LatLng(48.1907255, 16.3972948),
          zoom: 17,
		  scrollwheel: false,
		navigationControl: false,
		mapTypeControl: false,
		scaleControl: false,
		draggable: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
		
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(48.1907255, 16.3972948),
			map: map,
			title: 'Hello World!'
  });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
	
  </head><body>
    
	<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">AniFood</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
         <li><a href="#" class="scroll-link" data-id="idea">Idea</a>
                </li>
                <li><a href="#" class="scroll-link" data-id="team">Team</a>
                </li>
                <li><a href="#" class="scroll-link" data-id="goals">Goals</a>
                </li>
                <li><a href="#" class="scroll-link" data-id="doc">Documentation</a>
                </li>
				<li><a href="#" class="scroll-link" data-id="contact">Contact</a>
                </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php">Deutsch</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
  
    
	
    <div class="cover">
      
	  
	  
	  
	  
	  
      <div class="cover-image"></div>
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h1 class="navigation text-primary">ANIFOOD</h1>
            <p class="text-primary">Join the future! Feed your pets the comfortable way</p>
            <br>
            <br>
            <a class="btn btn-lg btn-primary" href="mailto:htl3r.anifood@gmail.com">KEEP IN TOUCH WITH US</a>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div id="fullcarousel-example1" data-interval="5000" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="item active">
                  <img src="images/toft01.jpg">
                  <!--<div class="carousel-caption">
                    <h2>Title</h2>
                    <p>Description</p>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6" id="idea">
            <h1 style="color:#337cbb">Project idea</h1>
            <h3>Current Situation</h3>
            <p>Many people are having pets at home – mostly cats and dogs – but it is hard to adapt your daily routine to your animals. Continuously they want to play with us or need food. If there was only a way to make it simpler, something like pressing a button and your pets gets its food. That’s where AniFood comes into play. It’s particularly designed for elder people, who are already forgetful and possibly may forget to feed their pet. Should they forget to feed their pet, our machine will provide food for the pet automatically. Your children and grandchildren won’t have to fear that their grandparents forget about feeding their pets.

			</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
        	
		</div>
          <div class="col-md-6">
            <h3>Description of the idea</h3>
            <p>AniFood isn‘t just a simple food dispenser, it is something more. The time, when the pet should be fed can easily be controlled and managed in the web interface. It does not matter if you are at home or not you can always access the web interface. Additionally, with the help of an integrated camera you are able to see, if your pet is actually eating its food. The amount of food can also be adjusted as required. If your pet should only get half of the amount it usually gets there will be a function to do that in the web interface, this is also one of many features our food dispenser AniFood provides.<br>

			A Raspberry PI together with a Wi-Fi module is responsible for communicating with the web interface. The data is transferred in real time and important information like food level or statistics are displayed in the web interface. The data is stored on a MySQL Database which is accessible through the web interface. You can also choose your cats/dogs breed to get further information on the recommended food intake.
      The food dispenser itself will be sturdy enough for even the roughest dog. You can easily take the bowl and the food holder out for cleaning purposes. The camera will face the bowl and will be completely integrated in the case.
			</p>
          </div>
          <div class="col-md-6">
            <div id="fullcarousel-example2" data-interval="5000" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                <div class="item active">
                  <img src="images/toft02.jpg">
                  <!--<div class="carousel-caption">
                    <h2>Title</h2>
                    <p>Description</p>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="team">
      <a name="team">
      </a>
      <div class="container">
        <a href="">
        </a>
        <div class="row">
          <a href="">
          </a>
          <div class="col-md-12">
            <a href="">
            </a>
            <a name="team"></a>
            <h1 class="text-center" style="color:#337cbb">Team</h1>
            <p class="text-center">We are a group of skilled individuals.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <img src="images/us/leon.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Leon Höfer</h3>
            <p class="text-center">Project Leader</p>
          </div>
          <div class="col-md-4">
            <img src="images/us/chris.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Christopher Wildzeisz</h3>
            <p class="text-center">Vice Projectleader</p>
          </div>
          <div class="col-md-4">
            <img src="images/us/weigl.png" class="center-block img-circle img-responsive">
            <h3 class="text-center">Mathias Weigl</h3>
            <p class="text-center">Developer</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section bar">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 style="color:#337cbb">Keep track of our Progress</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-right">
            <div class="active bar progress progress-striped">
              <div class="progress-bar progress-bar-info" role="progressbar" style="width: 60%;">
                <a href="#"></a>60% Complete</div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="goals">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="" class="center-block img-circle img-responsive">
          </div>
          <div class="col-md-6">
            <h1 style="color:#337cbb">Main Goals</h1>
            <h3>Main goal is the construction of a fully automatic, configurable food dispenser which can be monitored.</h3>
            <p>AniFood is a „box“. It contains: a Raspberry Pi and a small camera. It is also possible to remove the food bowl from the machine for cleaning.
			</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3> Main goal is coding a web interface which makes it possible to set the feeding time, show the amount of food in the bowl and statistics for the already consumed food.
			</h3>
            <p>A web interface is the basic foundation of our project. All information, such as: amount of food, feeding time, number of feeding times and statistics for the ingestion of food is stored in a database(which is located on the raspberry) and can be displayed in the interface.</p>
          </div>
          <div class="col-md-6">
            <img src="images/img01.jpg" class="img-circle img-responsive">
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <img src="" class="center-block img-circle img-responsive">
          </div>
          <div class="col-md-6">
            <h3>Main goal is building the „box“ ourselves and not buying already pre- built products.</h3>
            <p>The box should be built from a bite resistant material. The material itself
mustn’t have sharp edges or corners and needs to be water- and dirt proof.
			</p>
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="doc">
	<div class="container">
	<h1 style="color:#337cbb">Documentation</h1>
	<br><br>
      <a name="documents"></a>
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-md-offset-1">
            <a name="documents"></a>
            <a href="Downloads/AntragAniFood_SEPT_final_v6.pdf" download=""><img src="images/contract-icon.png" class="img-responsive img-thumbnail"></a>
            <h2>Download project contract</h2>
            <p>Download the approved project contract.</p>
          </div>
          <div class="col-md-3 col-md-offset-1">
            <a href="http://www.google.at"> <img src="images/1442285252_icon-70-document-file-pdf.png" class="img-responsive img-thumbnail"></a>
            <h2>Download weekly management summarys</h2>
            <p>Download weekly written management summarys to keep in depth track of
              our progress</p>
          </div>
          <div class="col-md-3 col-md-offset-1">
            <a href="http://www.google.at"> <img src="images/1442286506_icon-31-book-bookmark.png" class="img-responsive img-thumbnail"></a>
            <h2>Download project book</h2>
            <p>Download the finished project book to get an in depth look about the technologies
              used in our project</p>
          </div>
        </div>
      </div>
    </div>
	</div>
	
	<div id="map1">			
	    	</div>
		</div><!--/#map-->
	
    <footer class="section section-primary">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h1 >From humans for animals</h1>
            <ol class="list-inline">
              <li><h2>Impressum</h2>
				<p>Mathias Weigl<br>
					<br>
				Josef Anderlikgasse 9<br>
				2201<br>
				Gerasdorf<br>
				Österreich<br>
				<br>
				069917110625<br>
				htl3r.anifood@gmail.com<br>
				<br>
				</p>
			  <a href="tos.html" style="color:#FFF">Terms of Service </a></li>
            </ol>
          </div>
		  
		  
		  
          <div class="col-sm-6">
            <p class="footer text-left">
              <br>Find us on
              <b>:</b>
            </p>
            <div class="row">
              <div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
                <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
                <a href="#"><i class="fa fa-3x fa-fw fa-github text-inverse"></i></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 hidden-xs text-left">
                <!-- <a href="#"><i class="fa fa-3x fa-fw fa-instagram text-inverse"></i></a> -->
                <a href="https://twitter.com/htl3rAniFood"><i class="fa fa-3x fa-fw fa-twitter text-inverse"></i></a>
                <a href="https://www.facebook.com/htl3rAniFood"><i class="fa fa-3x fa-facebook fa-fw text-inverse"></i></a>
              </div>
            </div>
			<div class="row">
			
			<div id="contact">
	<h1 style="color:#FFFFFF">Contact</h1>
	<form class="form-horizontal" role="form" method="post" action="index.php">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="<?php echo htmlspecialchars($_POST['name']); ?>">
            <?php echo "<p class='text-danger'>$errName</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<?php echo htmlspecialchars($_POST['email']); ?>">
            <?php echo "<p class='text-danger'>$errEmail</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="message" class="col-sm-2 control-label">Message</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="4" name="message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
            <?php echo "<p class='text-danger'>$errMessage</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
            <?php echo "<p class='text-danger'>$errHuman</p>";?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary" style="background-color:#000000">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?php echo $result; ?>    
        </div>
    </div>
</form> 
	</div>
			
			</div>
          </div>
		  
        </div>
		
      </div>
	  
	  
    </footer>
  

</body></html>